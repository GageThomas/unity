﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour {


	public float speed;
	public Text countText;
	public Text winText;

	public Transform destination;
	public Transform Player;

	private int count;
	private Rigidbody rb;
	private GameObject gnd;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		count = 0;
		SetCountText ();
		winText.text = "";
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Pick Up"))
			{
				other.gameObject.SetActive (false);
			count = count + 1;
			SetCountText ();
			}

		if (other.tag == "Player") {
			gnd = GameObject.Find ("DeathGround");

			KillPlayer ();
		}

	}

	void KillPlayer ()
	{
		Player.transform.position = destination.position;
	}

	void SetCountText ()
	{
		countText.text = "Count: " + count.ToString ();
		if (count >= 23) {
			winText.text = "Completed.";
		}
	}
}